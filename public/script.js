// information about the author of the graphic
document.getElementById("profile-graphic").onclick = () => {
  alert(
    "Autorstwa Nidoart, CC BY-SA 3.0,\nhttps://commons.wikimedia.org/w/index.php?curid=23756701"
  );
};

// filling the stripes
let w = $(window);
let stripes = $("#stripes");
let fired = false;

let scroll = () => {
  if (!fired) {
    if (w.scrollTop() + w.height() > stripes.offset().top + 210) {
      $(".progress-bar").each((i, e) => {
        $(e).css("width", $(e).data("width") + "%");
      });

      fired = true;
    }
  }
};

scroll();

w.scroll(scroll);
